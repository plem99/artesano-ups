<?php
  session_start();
  if($_POST){
    include 'Tarifas_Logica.php';
  }
  
?>

<!DOCTYPE html>

<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-grid.css">
<link rel="stylesheet" href="css/bootstrap-grid.min.css">
<link rel="stylesheet" href="css/bootstrap-reboot.css">
<link rel="stylesheet" href="css/bootstrap-reboot.min.css">

<script src="js/bootstrap.bundle.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery-3.4.1.js"></script>

<!--Barra de Navegación-->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">ArtesanoMX</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link" href="Envios_Vista.php">Envíos</a>
      <a class="nav-item nav-link" href="Recoleccion_Vista.php">Recolección</a>
      <a class="nav-item nav-link active" href="Tarifas_Vista.php">Tarifas<span class="sr-only">(current)</span></a>
    </div>
  </div>
</nav>
  
  <div class="container" >
    <table class="table table-striped">
          <tbody>
             <tr>
                <td colspan="1">
                   <form class="well form-horizontal" method="post" >
                      <fieldset>
                        <div class="form-group">
                            <label class="col-md-4 control-label">*Opciones</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group">
                                  <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                  <select class="selectpicker form-control" name="T_opcion">
                                    <option value="Rate">Tarifa</option>
                                     <option value="Shop" selected>Shop</option>
                                     <option value="Ratetimeintransit">Tarifa de Tiempo en Tránsito</option>
                                     <option value="Shoptimeintransit">Shop de Tiempo en Tránsito</option>
                                  </select>
                               </div>
                            </div>
                         </div>
                          <div class="form-group">
                            <label class="col-md-4 control-label">*Tipo de Recolección</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group">
                                  <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                  <select class="selectpicker form-control" name="T_recoleccion">
                                    <option value="01">Recogida diaria</option>
                                     <option value="03">Contador de clientes</option>
                                     <option value="06">Recogida única</option>
                                     <option value="19">Centro de cartas</option>
                                     <option value="20">Centro de servicio aéreo</option>
                                  </select>
                               </div>
                            </div>
                         </div>
                         <label>----------------------------------------------------------</label>
                         <div class="form-group">
                            <label class="col-md-4 control-label">Nombre del Transportista</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="T_shipperName" name="T_shipperName" placeholder="Nombre del Transportista" class="form-control"  value="" type="text"></div>
                            </div>
                         </div>
                         <div class="form-group">
                            <label class="col-md-4 control-label">Número del Transportista</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="T_shipperNumber" name="T_shipperNumber" placeholder="Número del Transportista" class="form-control"  value="" type="text"></div>
                            </div>
                         </div>
                         <div class="form-group">
                            <label class="col-md-4 control-label">Dirección</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="T_shipperAddress" name="T_shipperAddress" placeholder="Dirección" class="form-control"  value="" type="text" ></div>
                            </div>
                         </div>
                         <div class="form-group">
                            <label class="col-md-4 control-label">Ciudad</label>
                            <div class="col-md-8 inputGroupContainer ">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="T_shipperCity" name="T_shipperCity" placeholder="Ciudad" class="form-control" required="true" value="San Pedro" type="text"  ></div>
                            </div>
                         </div>
                         <div class="form-group" action="Tarifas_Vista.php" method="post">
                            <label class="col-md-4 control-label">*País</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group">
                                  <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                  <select class="selectpicker form-control" name="T_shipperCountry" onChange="mostrar1(this.value);"  >
                                     <option value="MX" selected>México </option>
                                     <option value="US">Estados Unidos de America</option>
                                     <option value="CA">Canada</option>
                                  </select>
                               </div>
                            </div>
                         </div>
                         <div class="form-group">
                            <label class="col-md-4 control-label">*Código Postal/ZIP</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="T_shipperPc" name="T_shipperPc" placeholder="Código Postal/ZIP" class="form-control" required="true" value="66215" type="text"  ></div>
                            </div>
                         </div>
                         <div class="form-group">
                            <label class="col-md-4 control-label">*Estado</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group">
                                    <script type="text/javascript">
                                      function mostrar1(id) {
                                        if (id == "MX") {
                                          $("#MX1").show();
                                          $("#US1").hide();
                                          $("#CND1").hide();
                                        } else if(id == "US"){
                                          $("#MX1").hide();
                                          $("#US1").show();
                                          $("#CND1").hide();
                                        } else if(id == "CA"){
                                          $("#MX1").hide();
                                          $("#US1").hide();
                                          $("#CND1").show();
                                        }
                                      }
                                    </script>
                                    <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                    <div id="MX1">
                                      <select class="selectpicker form-control" name="T_shipperState" >
                                        <option value="">----Sin Seleccionar----</option>
                                       <option value="AG">Aguascalientes</option>
                                       <option value="BJ">Baja California</option>
                                       <option value="BS">Baja California Sur</option>
                                       <option value="CP">Campeche</option>
                                       <option value="CH">Chiapas</option>
                                       <option value="CI">Chihuahua</option>
                                       <option value="CU">Coahuila</option>
                                       <option value="CL">Colima</option>
                                       <option value="DF">CDMX</option>
                                       <option value="DG">Durango</option>
                                       <option value="GJ">Guanajuato</option>
                                       <option value="GR">Guerrero</option>
                                       <option value="HG">Hidalgo</option>
                                       <option value="JA">Jalisco</option>
                                       <option value="EM">México</option>
                                       <option value="MH">Michoacan</option>
                                       <option value="MR">Morelos</option>
                                       <option value="NA">Nayarit</option>
                                       <option value="NL" selected>Nuevo León</option>
                                       <option value="OA">Oaxaca</option>
                                       <option value="PU">Puebla</option>
                                       <option value="QA">Querétaro</option>
                                       <option value="QR">Quintana Roo</option>
                                       <option value="SL">San Luis Potósi</option>
                                       <option value="SI">Sinaloa</option>
                                       <option value="SO">Sonora</option>
                                       <option value="TA">Tabasco</option>
                                       <option value="TM">Tamaulipas</option>
                                       <option value="TL">Tlaxcala</option>
                                       <option value="VL">Veracruz</option>
                                       <option value="YC">Yucatán</option>
                                       <option value="ZT">Zacatecas</option>
                                      </select>
                                    </div>
                                    <div id="US1">
                                      <select class="selectpicker form-control" name="T_shipperState"  >
                                        <option value="">----Sin Seleccionar----</option>
                                        <option value="AL">Alabama </option>
                                        <option value="AK">Alaska </option>
                                        <option value="AZ">Arizona </option>
                                        <option value="AR">Arkansas </option>
                                        <option value="CA">California</option>
                                        <option value="NC">Carolina del Norte</option>
                                        <option value="SC">Carolina del Sur</option>
                                        <option value="CO">Colorado </option>
                                        <option value="CT">Conneticut</option>
                                        <option value="ND">Dakota del Norte</option>
                                        <option value="SD">Dakota del Sur</option>
                                        <option value="DC">Distrito de Columbia</option>
                                        <option value="FL">Florida </option>
                                        <option value="GA">Georgia </option>
                                        <option value="HI">Hawaii </option>
                                        <option value="ID">Idaho </option>
                                        <option value="IL">Illinois </option>
                                        <option value="IN">Indiana </option>
                                        <option value="IA">Iowa </option>
                                        <option value="KS">Kansas </option>
                                        <option value="KY">Kentucky </option>
                                        <option value="LA">Lousiana </option>
                                        <option value="ME">Maine </option>
                                        <option value="MD">Maryland </option>
                                        <option value="MA">Massachusetts </option>
                                        <option value="MI">Michigan </option>
                                        <option value="MN">Minnesota </option>
                                        <option value="MS">Mississippo </option>
                                        <option value="MO">Missouri </option>
                                        <option value="MT">Montana </option>
                                        <option value="NE">Nebraska </option>
                                        <option value="NV">Nevada </option>
                                        <option value="NH">New Hampshire</option>
                                        <option value="NJ">Nueva Jersey </option>
                                        <option value="NM">Nuevo México</option>
                                        <option value="NY">Nueva York</option>   
                                        <option value="OH">Ohio </option>
                                        <option value="OK">Oklahoma </option>
                                        <option value="OR">Oregon </option>
                                        <option value="PA">Pennsylvania </option>
                                        <option value="RI">Rhode Island </option>                             
                                        <option value="TN">Tennessee </option>
                                        <option value="TX">Texas </option>
                                        <option value="UT">Utah </option>
                                        <option value="VT">Vermont </option>
                                        <option value="VA">Virginia </option>
                                        <option value="WA">Washington </option>
                                        <option value="WV">Oeste Virginia</option>
                                        <option value="WI">Wisconsin </option>
                                        <option value="WY">Wyoming </option>
                                      </select>
                                    </div>
                                    <div id="CND1">
                                      <select class="selectpicker form-control" name="T_shipperState" >
                                        <option value="">----Sin Seleccionar----</option>
                                        <option value="CND1">Canada</option>
                                      </select>
                                    </div>
                               </div>
                            </div>
                         </div>
                         <label>----------------------------------------------------------</label>
                         <div class="form-group">
                            <label class="col-md-4 control-label">Nombre del Transportista a Envíar</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="T_shippertoName" name="T_shippertoName" placeholder="Nombre del Transportista" class="form-control"  value="" type="text"></div>
                            </div>
                         </div>
                         <div class="form-group">
                            <label class="col-md-4 control-label">Dirección</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="T_shippertoAddress" name="T_shippertoAddress" placeholder="Dirección" class="form-control"  value="" type="text"></div>
                            </div>
                         </div>
                         <div class="form-group">
                            <label class="col-md-4 control-label">Ciudad</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="T_shippertoCity" name="T_shippertoCity" placeholder="Ciudad" class="form-control" required="true" value="Newton" type="text"></div>
                            </div>
                         </div>
                         <div class="form-group" action="Tarifas_Vista.php" method="post">
                            <label class="col-md-4 control-label">*País</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group">
                                  <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                  <select class="selectpicker form-control" name="T_shippertoCountry" onChange="mostrar2(this.value);">
                                     <option value="MX">México </option>
                                     <option value="US" selected>Estados Unidos de America</option>
                                     <option value="CA">Canada</option>
                                  </select>
                               </div>
                            </div>
                         </div>
                         <div class="form-group">
                            <label class="col-md-4 control-label">*Código Postal/ZIP</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="T_shippertoPc" name="T_shippertoPc" placeholder="Código Postal/ZIP" class="form-control" required="true" value="06470" type="text"></div>
                            </div>
                         </div>
                         <div class="form-group">
                            <label class="col-md-4 control-label">*Estado</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group">
                                    <script type="text/javascript">
                                      function mostrar2(id) {
                                        if (id == "MX") {
                                          $("#MX2").show();
                                          $("#US2").hide();
                                          $("#CND2").hide();
                                        } else if(id == "US"){
                                          $("#MX2").hide();
                                          $("#US2").show();
                                          $("#CND2").hide();
                                        } else if(id == "CA"){
                                          $("#MX2").hide();
                                          $("#US2").hide();
                                          $("#CND2").show();
                                        }
                                      }
                                    </script>
                                    <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                    <div id="MX2">
                                      <select class="selectpicker form-control" name="T_shippertoState">
                                        <option value="">----Sin Seleccionar----</option>
                                       <option value="AG">Aguascalientes</option>
                                       <option value="BJ">Baja California</option>
                                       <option value="BS">Baja California Sur</option>
                                       <option value="CP">Campeche</option>
                                       <option value="CH">Chiapas</option>
                                       <option value="CI">Chihuahua</option>
                                       <option value="CU">Coahuila</option>
                                       <option value="CL">Colima</option>
                                       <option value="DF">CDMX</option>
                                       <option value="DG">Durango</option>
                                       <option value="GJ">Guanajuato</option>
                                       <option value="GR">Guerrero</option>
                                       <option value="HG">Hidalgo</option>
                                       <option value="JA">Jalisco</option>
                                       <option value="EM">México</option>
                                       <option value="MH">Michoacan</option>
                                       <option value="MR">Morelos</option>
                                       <option value="NA">Nayarit</option>
                                       <option value="NL">Nuevo León</option>
                                       <option value="OA">Oaxaca</option>
                                       <option value="PU">Puebla</option>
                                       <option value="QA">Querétaro</option>
                                       <option value="QR">Quintana Roo</option>
                                       <option value="SL">San Luis Potósi</option>
                                       <option value="SI">Sinaloa</option>
                                       <option value="SO">Sonora</option>
                                       <option value="TA">Tabasco</option>
                                       <option value="TM">Tamaulipas</option>
                                       <option value="TL">Tlaxcala</option>
                                       <option value="VL">Veracruz</option>
                                       <option value="YC">Yucatán</option>
                                       <option value="ZT">Zacatecas</option>
                                      </select>
                                    </div>
                                    <div id="US2">
                                      <select class="selectpicker form-control" name="T_shippertoState">
                                        <option value="">----Sin Seleccionar----</option>
                                        <option value="AL">Alabama </option>
                                        <option value="AK">Alaska </option>
                                        <option value="AZ">Arizona </option>
                                        <option value="AR">Arkansas </option>
                                        <option value="CA">California</option>
                                        <option value="NC">Carolina del Norte</option>
                                        <option value="SC">Carolina del Sur</option>
                                        <option value="CO">Colorado </option>
                                        <option value="CT" selected>Conneticut</option>
                                        <option value="ND">Dakota del Norte</option>
                                        <option value="SD">Dakota del Sur</option>
                                        <option value="DC">Distrito de Columbia</option>
                                        <option value="FL">Florida </option>
                                        <option value="GA">Georgia </option>
                                        <option value="HI">Hawaii </option>
                                        <option value="ID">Idaho </option>
                                        <option value="IL">Illinois </option>
                                        <option value="IN">Indiana </option>
                                        <option value="IA">Iowa </option>
                                        <option value="KS">Kansas </option>
                                        <option value="KY">Kentucky </option>
                                        <option value="LA">Lousiana </option>
                                        <option value="ME">Maine </option>
                                        <option value="MD">Maryland </option>
                                        <option value="MA">Massachusetts </option>
                                        <option value="MI">Michigan </option>
                                        <option value="MN">Minnesota </option>
                                        <option value="MS">Mississippo </option>
                                        <option value="MO">Missouri </option>
                                        <option value="MT">Montana </option>
                                        <option value="NE">Nebraska </option>
                                        <option value="NV">Nevada </option>
                                        <option value="NH">New Hampshire</option>
                                        <option value="NJ">Nueva Jersey </option>
                                        <option value="NM">Nuevo México</option>
                                        <option value="NY">Nueva York</option>   
                                        <option value="OH">Ohio </option>
                                        <option value="OK">Oklahoma </option>
                                        <option value="OR">Oregon </option>
                                        <option value="PA">Pennsylvania </option>
                                        <option value="RI">Rhode Island </option>                             
                                        <option value="TN">Tennessee </option>
                                        <option value="TX">Texas </option>
                                        <option value="UT">Utah </option>
                                        <option value="VT">Vermont </option>
                                        <option value="VA">Virginia </option>
                                        <option value="WA">Washington </option>
                                        <option value="WV">Oeste Virginia</option>
                                        <option value="WI">Wisconsin </option>
                                        <option value="WY">Wyoming </option>
                                      </select>
                                    </div>
                                    <div id="CND2">
                                      <select class="selectpicker form-control" name="T_shippertoState">
                                        <option value="">----Sin Seleccionar----</option>
                                        <option value="CND1">Canada</option>
                                      </select>
                                    </div>
                               </div>
                            </div>
                         </div>
                         <div class="form-group">
                            <label class="col-md-4 control-label">Referencias de Vivienda</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="T_shippertoRef" name="T_shippertoRef" placeholder="Referencias de Vivienda, ejemplo: Casa color cafe." class="form-control"  value="" type="text"></div>
                            </div>
                         </div>
                         <label>----------------------------------------------------------</label>
                         <div class="form-group">
                            <label class="col-md-4 control-label">Nombre del Transportista Donde Vino</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="T_shipperfromName" name="T_shipperfromName" placeholder="Nombre del Transportista" class="form-control"  value="" type="text"></div>
                            </div>
                         </div>
                         <div class="form-group">
                            <label class="col-md-4 control-label">Dirección</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="T_shipperfromAddress" name="T_shipperfromAddress" placeholder="Dirección" class="form-control"  value="" type="text"></div>
                            </div>
                         </div>
                         <div class="form-group">
                            <label class="col-md-4 control-label">Ciudad</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="T_shipperfromCity" name="T_shipperfromCity" placeholder="Ciudad" class="form-control" required="true" value="San Pedro" type="text"></div>
                            </div>
                         </div>
                         <div class="form-group" action="Tarifas_Vista.php" method="post">
                            <label class="col-md-4 control-label">*País</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group">
                                  <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                  <select class="selectpicker form-control" name="T_shipperfromCountry" onChange="mostrar3(this.value);">
                                     <option value="MX" selected>México </option>
                                     <option value="US">Estados Unidos de America</option>
                                     <option value="CA">Canada</option>
                                  </select>
                               </div>
                            </div>
                         </div>
                         <div class="form-group">
                            <label class="col-md-4 control-label">*Código Postal/ZIP</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="T_shipperfromPc" name="T_shipperfromPc" placeholder="Código Postal/ZIP" class="form-control" required="true" value="66215" type="text"></div>
                            </div>
                         </div>
                         <div class="form-group">
                            <label class="col-md-4 control-label">*Estado</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group">
                                    <script type="text/javascript">
                                      function mostrar3(id) {
                                        if (id == "MX") {
                                          $("#MX3").show();
                                          $("#US3").hide();
                                          $("#CND3").hide();
                                        } else if(id == "US"){
                                          $("#MX3").hide();
                                          $("#US3").show();
                                          $("#CND3").hide();
                                        } else if(id == "CA"){
                                          $("#MX3").hide();
                                          $("#US3").hide();
                                          $("#CND3").show();
                                        }
                                      }
                                    </script>
                                    <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                    <div id="MX3">
                                      <select class="selectpicker form-control" name="T_shipperfromState">
                                        <option value="">----Sin Seleccionar----</option>
                                       <option value="AG">Aguascalientes</option>
                                       <option value="BJ">Baja California</option>
                                       <option value="BS">Baja California Sur</option>
                                       <option value="CP">Campeche</option>
                                       <option value="CH">Chiapas</option>
                                       <option value="CI">Chihuahua</option>
                                       <option value="CU">Coahuila</option>
                                       <option value="CL">Colima</option>
                                       <option value="DF">CDMX</option>
                                       <option value="DG">Durango</option>
                                       <option value="GJ">Guanajuato</option>
                                       <option value="GR">Guerrero</option>
                                       <option value="HG">Hidalgo</option>
                                       <option value="JA">Jalisco</option>
                                       <option value="EM">México</option>
                                       <option value="MH">Michoacan</option>
                                       <option value="MR">Morelos</option>
                                       <option value="NA">Nayarit</option>
                                       <option value="NL" selected>Nuevo León</option>
                                       <option value="OA">Oaxaca</option>
                                       <option value="PU">Puebla</option>
                                       <option value="QA">Querétaro</option>
                                       <option value="QR">Quintana Roo</option>
                                       <option value="SL">San Luis Potósi</option>
                                       <option value="SI">Sinaloa</option>
                                       <option value="SO">Sonora</option>
                                       <option value="TA">Tabasco</option>
                                       <option value="TM">Tamaulipas</option>
                                       <option value="TL">Tlaxcala</option>
                                       <option value="VL">Veracruz</option>
                                       <option value="YC">Yucatán</option>
                                       <option value="ZT">Zacatecas</option>
                                      </select>
                                    </div>
                                    <div id="US3">
                                      <select class="selectpicker form-control" name="T_shipperfromState">
                                        <option value="">----Sin Seleccionar----</option>
                                        <option value="AL">Alabama </option>
                                        <option value="AK">Alaska </option>
                                        <option value="AZ">Arizona </option>
                                        <option value="AR">Arkansas </option>
                                        <option value="CA">California</option>
                                        <option value="NC">Carolina del Norte</option>
                                        <option value="SC">Carolina del Sur</option>
                                        <option value="CO">Colorado </option>
                                        <option value="CT">Conneticut</option>
                                        <option value="ND">Dakota del Norte</option>
                                        <option value="SD">Dakota del Sur</option>
                                        <option value="DC">Distrito de Columbia</option>
                                        <option value="FL">Florida </option>
                                        <option value="GA">Georgia </option>
                                        <option value="HI">Hawaii </option>
                                        <option value="ID">Idaho </option>
                                        <option value="IL">Illinois </option>
                                        <option value="IN">Indiana </option>
                                        <option value="IA">Iowa </option>
                                        <option value="KS">Kansas </option>
                                        <option value="KY">Kentucky </option>
                                        <option value="LA">Lousiana </option>
                                        <option value="ME">Maine </option>
                                        <option value="MD">Maryland </option>
                                        <option value="MA">Massachusetts </option>
                                        <option value="MI">Michigan </option>
                                        <option value="MN">Minnesota </option>
                                        <option value="MS">Mississippo </option>
                                        <option value="MO">Missouri </option>
                                        <option value="MT">Montana </option>
                                        <option value="NE">Nebraska </option>
                                        <option value="NV">Nevada </option>
                                        <option value="NH">New Hampshire</option>
                                        <option value="NJ">Nueva Jersey </option>
                                        <option value="NM">Nuevo México</option>
                                        <option value="NY">Nueva York</option>   
                                        <option value="OH">Ohio </option>
                                        <option value="OK">Oklahoma </option>
                                        <option value="OR">Oregon </option>
                                        <option value="PA">Pennsylvania </option>
                                        <option value="RI">Rhode Island </option>                             
                                        <option value="TN">Tennessee </option>
                                        <option value="TX">Texas </option>
                                        <option value="UT">Utah </option>
                                        <option value="VT">Vermont </option>
                                        <option value="VA">Virginia </option>
                                        <option value="WA">Washington </option>
                                        <option value="WV">Oeste Virginia</option>
                                        <option value="WI">Wisconsin </option>
                                        <option value="WY">Wyoming </option>
                                      </select>
                                    </div>
                                    <div id="CND3">
                                      <select class="selectpicker form-control" name="T_shipperfromState">
                                        <option value="">----Sin Seleccionar----</option>
                                        <option value="CND1">Canada</option>
                                      </select>
                                    </div>
                               </div>
                            </div>
                         </div>
                         <label>----------------------------------------------------------</label>
                         <div class="form-group">
                            <label class="col-md-4 control-label">*Servicio</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group">
                                  <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                  <select class="selectpicker form-control" name="T_servicio">
                                     <option value="70">UPS Access Point Economy</option>
                                     <option value="08">UPS Expedited </option>
                                     <option value="07">UPS Exprés </option>
                                     <option value="11" selected>UPS Estándar</option>
                                     <option value="54">UPS Worldwide Express Plus</option>
                                     <option value="65">UPS Worldwide Saver</option>
                                  </select>
                               </div>
                            </div>
                         </div>
                         <div class="form-group">
                            <label class="col-md-4 control-label">*Tipo de Paquete</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group">
                                  <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                  <select class="selectpicker form-control" name="T_paquete">
                                     <option value="00">Desconocido </option>
                                     <option value="01">UPS Carta </option>
                                     <option value="02" selected="">Paquete </option>
                                     <option value="03">Tubo </option>
                                     <option value="04">Pak </option>
                                     <option value="21">Caja Exprés </option>
                                     <option value="24">Caja de 25KG </option>
                                     <option value="25">Caja de 10KG </option>
                                     <option value="30">Pallet </option>
                                     <option value="2a">Caja Exprés Chica </option>
                                     <option value="2b">Caja Exprés Mediana </option>
                                     <option value="2c">Caja Exprés Grande </option>
                                  </select>
                               </div>
                            </div>
                         </div>
                       <div class="form-group">
                            <div class="col-md-8 inputGroupContainer">
                            <label class="col-md-4 control-label">*Longitud</label>
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="T_long" name="T_long" placeholder="Longitud" class="form-control" required="true" value="10" type="text">

                            <div class="col-md-8 inputGroupContainer">
                            <label class="col-md-4 control-label">*Ancho</label>
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="T_anch" name="T_anch" placeholder="Ancho" class="form-control" required="true" value="10" type="text">
                            
                            <div class="col-md-8 inputGroupContainer">
                            <label class="col-md-4 control-label">*Alto</label>
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="T_alto" name="T_alto" placeholder="Alto" class="form-control" required="true" value="10" type="text">
                            

                               <span class="input-group-addon" style="max-width: 50%;"><i class="glyphicon glyphicon-list"></i></span>
                                  <select class="selectpicker form-control" name="T_medida">
                                     <option value="CM">Centimetros</option>
                                     <option value="IN">Pulgadas</option>
                                  </select>

                                  </div>
                                  </div>
                                  </div>
                            </div> 
                            </div> 
                            </div>   
                         </div>
                         <div class="form-group">
                            <label class="col-md-4 control-label">*Peso</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="T_peso" name="T_peso" placeholder="Peso" class="form-control" required="true" value="10" type="text">
                               <span class="input-group-addon" style="max-width: 50%;"><i class="glyphicon glyphicon-list"></i></span>
                                  <select class="selectpicker form-control" name="T_tipoPedo">
                                     <option value="KGS" selected="">Kilogramos</option>
                                     <option value="LBS">Libras</option>
                                  </select>
                                </div>
                            </div>
                         </div>

                      </fieldset>
                      <button type="submit" value="Entrar" id="enviar" class="btn btn-success">Enviar</button>
                   </form>
                </td>
                </tbody>
                </table>
  </div>



</html>