<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Artesano</title>
    <!-- BOOSTRAP 4 -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!--Iconos para el sitio-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--Hojas de estilo-->
    <link rel="stylesheet" type="text/css" href='css/estiloPaquete.css'>
    <link rel="stylesheet" href="w3css/w3.css">
    <link rel="stylesheet" href="w3css/w4.css">
    <!--Icono de la pestaña-->
	<link rel="ICON"  type="iMAGEN/PNG" href="img/LogoPRD.png">
</head>
<body>