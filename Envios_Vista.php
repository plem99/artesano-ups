<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SIC Artesano</title>
    <!--Hojas de estilo-->
    <link rel="stylesheet" href="w3css/w3.css">
    <link rel="stylesheet" type="text/css" href='css/estiloNav.css'>
    <link rel="stylesheet" type="text/css" href='css/estiloUPS.css'>
    <!--Iconos para el sitio-->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--Icono de la pestaña-->
	<link rel="ICON"  type="iMAGEN/PNG" href="img/LogoPRD.png">
</head>
<body>
<!--Estructura del menu SIC-->
<div class="topnav" id="myTopnav">
  <a class="w3-padding-16 w3-text-white logo"><b>SIC ArtesanoMX</b></a>
  <a href="index.php" class="w3-bar-item w3-button w3-padding-16">Productos</a>
  <a href="formulario.php" class="w3-bar-item w3-button w3-padding-16">Agregar producto</a>
  <a href="historial.php" class="w3-bar-item w3-button w3-padding-16">Historial</a>
  <a href="javascript:void(0);" class="icon" onclick="Responsive()">
  <i class="fa fa-bars"></i></a>
</div>

<!--Script para activar el modo responsive-->
<script>
function Responsive() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}
</script>


<!--Contenido 1-->
<div class="w3-row">
    <div class="w3-rest w3-container bgGris">
        <h5><span class="w3-badge">1</span>¿A dónde va este envío?</h5>
    </div>
</div>
<div class="w3-row bgBlanco w3-padding-16">
    <div class="w3-container w3-padding-16">
    <!--Dirección-->
        <label><b>Dirección:</b></label> 
        <select class="form-control w3-border w3-round-large selector" required>
            <option value="UPS Leones">UPS Leones</option>
            <option value="Unknown">Unknown</option>
        </select>
        --- o ingrese una nueva dirección a continuación
    </div>

    <!--Botón Editar-->
    <div class="w3-container w3-padding-16">
            <b>Envie a:</b>
            <button class="w3-button w3-teal w3-round-large"><u>Editar</u></button>
    </div>

    <div class="w3-container">
        ARTESANO
    </div>
    <div class="w3-container">
        Ricardo Amaya
    </div>
    <div class="w3-container">
        Av Paseo de los Leones #832
    </div>
    <div class="w3-container">
        Cumbres 1°. Sector Secc a
    </div>
    <div class="w3-container">
        64610 Monterrey
    </div>
    <div class="w3-container">
        Teléfono: 8341067772
    </div>
    <div class="w3-container">
        Email: valerysoto@ups.com
    </div>
</div>


<!--Contenido 2-->
<div class="w3-row">
    <div class="w3-rest w3-container bgGris">
        <h5><span class="w3-badge">2</span>¿De dónde viene este envío?</h5>
    </div>
</div>

<div class="w3-row bgBlanco">
  <div class="w3-half w3-container">
        <!--Botón Editar-->
        <div class="w3-container w3-padding-16">
            <b>Enviar desde la dirección:</b>
            <button class="w3-button w3-teal w3-round-large"><u>Editar</u></button>
        </div>

    <!--Columna 1 : Datos personales-->
    <div class="w3-container">
        RICARDO AMAYA ESCOBEDO
    </div>
    <div class="w3-container">
        Ricardo Amaya
    </div>
    <div class="w3-container">
        CALIOPE #106
    </div>
    <div class="w3-container">
        PRIVALIA CUMBRES
    </div>
    <div class="w3-container">
        66036 GARCIA
    </div>
    <div class="w3-container">
        Teléfono: 8341231201
    </div>
    <div class="w3-container">
        Email: valerysoto@ups.com
    </div>
  </div>

  <!--Columna 2: Selectores-->
  <div class="w3-half w3-container  w3-padding-16">
        <div class="w3-container">
            Si el envío no se puede entregar, regrese a:
        </div>
        <div class="w3-container espacio">
            <label><b>Contacto:</b></label> 
       </div>
        <div class="w3-container">
            <select class="form-control w3-border w3-round-large selector" required>
                <option value="UPS Leones">Ricardo Amaya</option>
                <option value="Unknown">Unknown</option>
            </select>
       </div>

       <div class="w3-container espacio">
            <label><b><u>Dirección del remitente:</u></b></label>
       </div>
       <div class="w3-container">
            <select class="form-control w3-border w3-round-large selector" required>
                <option value="UPS Leones">Same As Ship From</option>
                <option value="Unknown">Unknown</option>
            </select>
       </div>
  </div>
</div>


<!--Contenido 3-->
<div class="w3-row">
    <div class="w3-rest w3-container bgGris">
        <h5><span class="w3-badge">3</span>¿Qué estás enviando?</h5>
    </div>
</div>

<div class="w3-row bgBlanco">
    <!--Numero de paquetes-->
   <div class="w3-container w3-padding-16">
        <label><b>Número de paquetes:</b></label> 
        <input class="w3-input w3-round-large w3-border" type="number" placeholder="0" min="0" required>
    </div>
    <!--Tipo de paquetado-->
    <div class="w3-container espacio">
         <label><b>Tipo de paquetes: </b></label> 
    </div>
    <div class="w3-container">
        <select class="form-control w3-border w3-round-large selector" required>
            <option value="UPS Leones">UPS Express Box</option>
            <option value="Unknown">Unknown</option>
        </select>
    </div>

    <!--Peso del envio-->
   <div class="w3-container w3-padding-16">
        <label><b>Peso del envio: (kg)*</b></label> 
        <input class="w3-input w3-round-large w3-border" type="number" placeholder="0" min="0" required>
    </div>

    <!--Dimensiones del paquete-->
    <div class="w3-container">
        <b>Dimensiones del paquete:</b>
    </div>
    <!--Langitud-->
  <div class="w3-third w3-container w3-padding-16">
    <div class="w3-container">
         <label><b>Longitud: (cm)*</b></label>
         <input class="w3-input w3-round-large w3-border" type="number" placeholder="0" min="0" required>
      </div>
  </div>
  <!--Anchura-->
  <div class="w3-third w3-container w3-padding-16">
    <div class="w3-container">
         <label><b>Ancho: (cm)*</b></label>
         <input class="w3-input w3-round-large w3-border" type="number" placeholder="0" min="0" required>
      </div>
  </div>
  <!--Altura-->
  <div class="w3-third w3-container w3-padding-16">
    <div class="w3-container">
         <label><b>Altura: (cm)*</b></label>
         <input class="w3-input w3-round-large w3-border" type="number" placeholder="0" min="0" required>
    </div>
  </div>
    <!--Valor-->
    <div class="w3-container w3-padding-16">
        <label><b>Envío Valor declarado: (MXN)</b></label> 
        <input class="w3-input w3-round-large w3-border" type="number" placeholder="0" min="0" required>
    </div>
</div>

<!--Contenido 4-->
<div class="w3-row">
    <div class="w3-rest w3-container bgGris">
        <h5><span class="w3-badge">4</span>¿Cómo te gustaría enviar?</h5>
    </div>
</div>
<div class="w3-row bgBlanco w3-padding-16">
    <div class="w3-container w3-padding-16">
    <!--Servicio-->
        <label><b>Servicio:</b></label> 
        <select class="form-control w3-border w3-round-large selector" required>
            <option value="UPS Leones">UPS Express Saver</option>
            <option value="Unknown">Unknown</option>
        </select>
         <u>Comparar tiempo y costo</u>
    </div>

    <div class="w3-container w3-padding-16">
        <b>¿Necesitas servicios adicionales?</b>
    </div>
    <div class="w3-container">
        <input class="w3-check" type="checkbox">
        <label>¿Enviar notificaciones por correo electrónico?</label>
    </div>
    <div class="w3-container">
         <input class="w3-check" type="checkbox">
        <label>Recibir confirmación de entrega</label>
    </div>
    <div class="w3-container">
         <input class="w3-check" type="checkbox">
        <label>C.O.D</label>
    </div>
</div>



<!--Contenido 5-->
<div class="w3-row">
    <div class="w3-rest w3-container bgGris">
        <h5><span class="w3-badge">5</span> ¿Desea agregar números de referencia a este envío?</h5>
    </div>
</div>
<!--Referencias-->
<div class="w3-row bgBlanco w3-padding-16">
    <div class="w3-container w3-padding-16">
        UPS le brinda la opción de rastrear sus envíos utilizando las referencias que defina.
    </div>
    <div class="w3-container">
        <label><b>Refencia #1:</b></label> 
        <input class="w3-input w3-round-large w3-border" type="text" placeholder="Ingresa una referencia" required>
    </div>
    <div class="w3-container espacio">
        <label><b>Refencia #2:</b></label> 
        <input class="w3-input w3-round-large w3-border" type="text" placeholder="Ingresa una referencia" required>
    </div>
</div>


</body>
