<!DOCTYPE html>

<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
<script integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="></script>
<script crossorigin="anonymous"></script>

<?php
  //include 'Tarifas_Datos.php';
  //Configuración
  $access = "ED715201D9516E11";
  $userid = "artesanomx";
  $passwd = "Mexico-05";
  $wsdl = "C:\Users\Pedro Martinez\Documents\Artesanomx\Api UPS\Api Tarifas\RatingPACKAGE\PACKAGEWebServices\SCHEMA-WSDLs\RateWS.wsdl";
  $operation = "ProcessRate";
  $endpointurl = 'https://wwwcie.ups.com/webservices/Rate';
  $outputFileName = "XOLTResult.xml";

  function asigVariables(){
    
  }

   function processRate(){
      //Opciones
      $opcion=$_POST["T_opcion"]; 
      //TipoRecolección
      $recoleccion=$_POST["T_recoleccion"]; 
      //Transportista
      $transportistaNomb = array($_POST["T_shipperName"], $_POST["T_shippertoName"], $_POST["T_shipperfromName"]);
      $transportistaNum=$_POST["T_shipperNumber"];
      $transportistaDirecc = array($_POST["T_shipperAddress"], $_POST["T_shippertoAddress"], $_POST["T_shipperfromAddress"]);
      $transportistaCiudad = array($_POST["T_shipperCity"], $_POST["T_shippertoCity"], $_POST["T_shipperfromCity"]);
      $transportistaPais = array($_POST["T_shipperCountry"], $_POST["T_shippertoCountry"], $_POST["T_shipperfromCountry"]);
      $transportistaCodPost = array($_POST["T_shipperPc"], $_POST["T_shippertoPc"], $_POST["T_shipperfromPc"]);
      $transportistaEstado = array($_POST["T_shipperState"], $_POST["T_shippertoState"], $_POST["T_shipperfromState"]);
      $transportistaRef=$_POST["T_shippertoRef"];
      //Servicios
      $servicios=$_POST["T_servicio"];
      $paquete=$_POST["T_paquete"];
      $longitud=$_POST["T_long"];
      $ancho=$_POST["T_anch"];
      $altura=$_POST["T_alto"];
      $medida=$_POST["T_medida"];
      //Peso
      $peso=$_POST["T_peso"];
      $tipoPeso=$_POST["T_tipoPedo"];

      //Imprimir valores obtenidos
      /*echo "Opcion: " . var_dump($opcion) . "\n";
      echo "Tipo de Recolección: " . var_dump($recoleccion) . "\n";
      echo "Nombre del Transportista: " . var_dump($transportistaNomb) . "\n";
      echo "Número del Transportista: " . var_dump($transportistaNum) . "\n";
      echo "Dirección del Transportista: " . var_dump($transportistaDirecc) . "\n";
      echo "Ciudad del Transportista: " . var_dump($transportistaCiudad) . "\n";
      echo "Pais del Transportista: " . var_dump($transportistaPais) . "\n";
      echo "Código Postal del Transportista: " . var_dump($transportistaCodPost) . "\n";
      echo "Estado del Transportista: " . var_dump($transportistaEstado) . "\n";
      echo "Referencias del Envío: " . var_dump($transportistaRef) . "\n";
      echo "Tipo de Servicio: " . var_dump($servicios) . "\n";
      echo "Tipo de Paquete: " . var_dump($paquete) . "\n";
      echo "Longitud: " . var_dump($longitud) . "\n";
      echo "Ancho: " . var_dump($ancho) . "\n";
      echo "Altura: " . var_dump($altura) . "\n";
      echo "Tipo de Medida: " . var_dump($medida) . "\n";
      echo "Peso: " . var_dump($peso) . "\n";
      echo "Tipo de Peso: " . var_dump($tipoPeso) . "\n";*/
      
      //create soap request
      //Opción
      $option['RequestOption'] = $opcion;
      $request['Request'] = $option;

      //Tipo de recolección
      $pickuptype['Code'] = $recoleccion;
      $pickuptype['Description'] = 'Opcion de Recolección';
      $request['PickupType'] = $pickuptype;

      //Opcional
      $customerclassification['Code'] = '01';
      $customerclassification['Description'] = 'Classfication';
      $request['CustomerClassification'] = $customerclassification;

      //Transportista
      $shipper['Name'] = $transportistaNomb[0];
      $shipper['ShipperNumber'] = $transportistaNum;
      $address['AddressLine'] = $transportistaDirecc[0];
      $address['City'] = $transportistaCiudad[0];
      $address['StateProvinceCode'] = $transportistaEstado[0];
      $address['PostalCode'] = $transportistaCodPost[0];
      $address['CountryCode'] = $transportistaPais[0];
      $shipper['Address'] = $address;
      $shipment['Shipper'] = $shipper;

      //A Donde se Enviara
      $shipto['Name'] = $transportistaNomb[1];
      $addressTo['AddressLine'] = $transportistaDirecc[1];
      $addressTo['City'] = $transportistaCiudad[1];
      $addressTo['StateProvinceCode'] = $transportistaEstado[1];
      $addressTo['PostalCode'] = $transportistaCodPost[1];
      $addressTo['CountryCode'] = $transportistaPais[1];
      $addressTo['ResidentialAddressIndicator'] = $transportistaRef;
      $shipto['Address'] = $addressTo;
      $shipment['ShipTo'] = $shipto;

      //De donde Vino
      $shipfrom['Name'] = $transportistaNomb[2];
      $addressFrom['AddressLine'] = $transportistaDirecc[2];
      $addressFrom['City'] = $transportistaCiudad[2];
      $addressFrom['StateProvinceCode'] = $transportistaEstado[2];
      $addressFrom['PostalCode'] = $transportistaCodPost[2];
      $addressFrom['CountryCode'] = $transportistaPais[2];
      $shipfrom['Address'] = $addressFrom;
      $shipment['ShipFrom'] = $shipfrom;

      //Servicio (Plan de Envío)
      $service['Code'] = $servicios;
      $service['Description'] = 'Código del Servicio';
      $shipment['Service'] = $service;

      //Tipo de Paquete a enviar
      $packaging1['Code'] = $paquete;
      $packaging1['Description'] = 'Rate';
      $package1['PackagingType'] = $packaging1;
      $dunit1['Code'] = $medida;
      $dunit1['Description'] = 'Tipo de Medición';
      $dimensions1['Length'] = $longitud;
      $dimensions1['Width'] = $ancho;
      $dimensions1['Height'] = $altura;
      $dimensions1['UnitOfMeasurement'] = $dunit1;
      $package1['Dimensions'] = $dimensions1;
      $punit1['Code'] = $tipoPeso;
      $punit1['Description'] = 'Tipo de Peso';
      $packageweight1['Weight'] = $peso;
      $packageweight1['UnitOfMeasurement'] = $punit1;
      $package1['PackageWeight'] = $packageweight1;

/*
      $packaging2['Code'] = '02';
      $packaging2['Description'] = 'Rate';
      $package2['PackagingType'] = $packaging2;
      $dunit2['Code'] = 'IN';
      $dunit2['Description'] = 'inches';
      $dimensions2['Length'] = '3';
      $dimensions2['Width'] = '5';
      $dimensions2['Height'] = '8';
      $dimensions2['UnitOfMeasurement'] = $dunit2;
      $package2['Dimensions'] = $dimensions2;
      $punit2['Code'] = 'LBS';
      $punit2['Description'] = 'Pounds';
      $packageweight2['Weight'] = '2';
      $packageweight2['UnitOfMeasurement'] = $punit2;
      $package2['PackageWeight'] = $packageweight2;
*/
      $shipment['Package'] = array( $package1 /*, $package2 */);
      $shipment['ShipmentServiceOptions'] = '';
      $shipment['LargePackageIndicator'] = '';
      $request['Shipment'] = $shipment;
      //echo "Request.......\n";
      //print_r($request);
      //echo "\n\n";
      return $request;
  }

  try
  {

    $mode = array
    (
         'soap_version' => 'SOAP_1_1',  // use soap 1.1 client
         'trace' => 1
    );

    // initialize soap client
    $client = new SoapClient($wsdl , $mode);

    //set endpoint url
    $client->__setLocation($endpointurl);


    //create soap header
    $usernameToken['Username'] = $userid;
    $usernameToken['Password'] = $passwd;
    $serviceAccessLicense['AccessLicenseNumber'] = $access;
    $upss['UsernameToken'] = $usernameToken;
    $upss['ServiceAccessToken'] = $serviceAccessLicense;

    $header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0','UPSSecurity',$upss);
    $client->__setSoapHeaders($header);


    //get response
    $resp = $client->__soapCall($operation ,array(processRate()));

    //get status
    if ($resp->Response->ResponseStatus->Description) {
      echo "\n\n";
      echo "Prueba Satisfactoria"."\n";
      ?>
      <!-- Button trigger modal -->
      <button type="button" class="btn btn-success" data-toggle="modal" data-target=".bd-example-modal-lg">
        Ver Tarifa
      </button>
      <div data-toggle="modal" data-target=".bd-example-modal-lg"></div>

      <!-- Modal -->
      <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Tarifas</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <ul class="list-group">
              <!--Muestro los datos-->
                <?php 
                  $i=0;
                  $numOpcion=1;

                  while($resp->RatedShipment[$i]->TotalCharges) { 
                    //if(!$resp->RatedShipment[$i]->TotalCharges){
                    //  break;
                    //}
                ?>
                  <?php echo "Opcion ".$numOpcion."\n"?>
                  <li class="list-group-item">
                    <?php 
                    $servicioTipo=$resp->RatedShipment[$i]->Service->Code;
                    $valor;
                    switch ($servicioTipo) {
                      case '70':
                        $valor='UPS Access Point Economy';
                      break;
                      case '08':
                        $valor='UPS Expedited';
                      break;
                      case '07':
                        $valor='UPS Exprés';
                      break;
                      case '11':
                        $valor='UPS Estándar';
                      break;
                      case '54':
                        $valor='UPS Worldwide Express Plus';
                      break;
                      case '65':
                        $valor='UPS Worldwide Saver';
                      break;
                    }
                    echo "Servicio: " . $valor."\n"; 
                    ?>
                  </li>
                  <li class="list-group-item">
                    <?php echo "Cargos por Transporte: $" . $resp->RatedShipment[$i]->TransportationCharges->MonetaryValue . " " . $resp->RatedShipment[$i]->TotalCharges->CurrencyCode . "\n"; ?>
                  </li>
                  <li class="list-group-item">
                    <?php echo "Cargos por Servicio: $" . $resp->RatedShipment[$i]->ServiceOptionsCharges->MonetaryValue . " " . $resp->RatedShipment[$i]->TotalCharges->CurrencyCode . "\n"; ?>
                  </li>
                  <li class="list-group-item">
                    <?php echo "El total a Pagar es: $" . $resp->RatedShipment[$i]->TotalCharges->MonetaryValue . " " . $resp->RatedShipment[$i]->TotalCharges->CurrencyCode . "\n"; ?>
                  </li>
                  <li>-----------------------------------------</li>
                <?php 
                  $i++;
                  $numOpcion++;
                } 

                ?>
            </ul>
            </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
          </div>
          </div>
        </div>
      </div>

    <?php
      //echo $resp->Response->ResponseStatus->Code;
    }else{
      echo "Prueba Fallida\n";
    }
    //echo "Response Status: " . $resp->Response->ResponseStatus->Description ."\n";

    //save soap request and response to file
    $fw = fopen($outputFileName , 'w');
    fwrite($fw , "Request: \n" . $client->__getLastRequest() . "\n");
    fwrite($fw , "Response: \n" . $client->__getLastResponse() . "\n");
    fclose($fw);

  }
  catch(Exception $ex)
  {
    print_r ($ex);
  }
?>
</html>