<?php 
include("includes/headerPaquete.php");
session_start();
$val=0;
  if($_POST){
    $val=1;
    $nac=1;
    include 'Tarifas_Logica2.php';
  }
?>

<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-grid.css">
<link rel="stylesheet" href="css/bootstrap-grid.min.css">
<link rel="stylesheet" href="css/bootstrap-reboot.css">
<link rel="stylesheet" href="css/bootstrap-reboot.min.css">

<script src="js/bootstrap.bundle.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery-3.4.1.js"></script>

<!--Barra de Navegación-->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">ArtesanoMX</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link" href="">Envíos</a>
      <a class="nav-item nav-link active" href="Tarifas_Nacional.php">Tarifas<span class="sr-only">(current)</span></a>
    </div>
  </div>
</nav>


<div class="w3-row-padding" >
<!--<div class="w3-third w3-container">-->

<!--Columna Nacional-->
<div class="w3-row-padding" >
  <ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link active" href="Tarifas_Nacional.php">Nacional</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="Tarifas_Internacional.php">Internacional</a>
  </li>
</ul>

<!--</div>-->

<!--Columna Nacional-->
<form method="post" >
<!--Columna 1-->
<p>
  <div class="w3-third w3-container">

    <div class="w3-row bgGris w3-padding-16">
      <div class="w3-container">
         <h5 class="txtGris"><b>Dimensiones del paquete</b></h5>
      </div>
    </div>

    <!--Campo codigo postal de origen-->
    <div class="w3-row bgGris w3-padding-16">
      <div class="w3-container">
        <label>Código postal de origen*</label>
         <input class="w3-input w3-round-large w3-border" min="00001" max="99999" minlength="5" maxlength="5" type="number" placeholder="Código postal de origen" id="T_shipperfromPc" name="T_shipperfromPc" value="" required>
      </div>
    </div>

    <!--Campo codigo postal de destino-->
    <div class="w3-row bgGris w3-padding-16">
      <div class="w3-container">
        <label>Código postal de destino*</label>
         <input class="w3-input w3-border w3-round-large " min="00001" max="99999" minlength="5" maxlength="5" type="number" placeholder="Código postal de destino" id="T_shippertoPc" name="T_shippertoPc" value="" required>
      </div>
    </div>

    <div class="w3-row bgGris w3-padding-16">
        <!--Campo de largo-->
      <div class="w3-half w3-container">
        <label>Largo (CM)*</label>
         <input class="w3-input w3-border w3-round-large " type="number" placeholder="Largo (CM)" min="1" max="80" minlength="1" maxlength="2" id="T_long" name="T_long" value="" required>
      </div>
        <!--Campo de alto-->
      <div class="w3-half w3-container">
        <label>Alto (CM)*</label>
         <input class="w3-input w3-border w3-round-large " type="number" placeholder="Alto (CM)" min="1" max="80" minlength="1" maxlength="2" id="T_alto" name="T_alto" value="" required>
      </div>
    </div>

    <div class="w3-row bgGris w3-padding-16 ">
      <!--Campo de ancho-->
      <div class="w3-half w3-container">
        <label>Ancho (CM)*</label>
         <input class="w3-input w3-border w3-round-large " type="number" placeholder="Ancho (CM)" min="1" max="80" minlength="1" maxlength="2" id="T_anch" name="T_anch" value="" required>
      </div>
      <!--Campo de peso-->
      <div class="w3-half w3-container">
        <label>Peso (KG)*</label>
         <input class="w3-input w3-border w3-round-large " type="number" placeholder="Peso (KG)" min="1" max="70" minlength="1" maxlength="2" id="T_peso" name="T_peso" value="" required>
      </div>
    </div>

    <!--Peso-->
    <div class="w3-row bgGris w3-padding-16">
      <div class="w3-container">
        <?php if($val){ ?>
          <label> Peso Masa: <b><?php echo $_POST["T_peso"] . " Kg.";?></label></b><br>
          <label> Peso Volumétrico: <b><?php echo pesoVol($_POST["T_long"], $_POST["T_alto"], $_POST["T_anch"]) . " Kg.";?></label></b><br>
          <?php if($_POST["T_peso"]>pesoVol($_POST["T_long"], $_POST["T_alto"], $_POST["T_anch"])){ ?>
            <label> Peso a Cotizar: <b><?php echo $_POST["T_peso"] . " Kg.";?></label></b><br>
          <?php }else{ ?>
            <label> Peso a Cotizar: <b><?php echo pesoVol($_POST["T_long"], $_POST["T_alto"], $_POST["T_anch"]). " Kg.";?></label></b><br>
          <?php }?>   
        <?php }?>
      </div>
    </div>
    
    <!--Botón cotizar-->
    <div class="w3-row bgGris w3-padding-16 ">
      <div class="w3-container">
        <button class="w3-button w3-block w3-teal w3-round-large" type="submit" value="Entrar" id="enviar">Cotizar</button>
      </div>
    </div>

  </div>
</form>


  <!--Columna 2-->
  <div class="w3-rest w3-container">

    <div class="w3-row bgGris w3-padding-16">
        <div class="w3-container">
          <h5 class="txtGris"><b>Servicios disponibles</b></h5>
        </div>
    </div>
    <div class="w3-row bgGris txtGris w3-padding-16 w3-responsive">
      <!--Visualización de la tabla-->
      <table class="w3-table tableGris alinear thGris">
      <tr>
        <th>Paquetería</th>
        <th>Servicio</th>
        <!--<th>Entrega estimada</th>-->
        <th>Precio</th>
        <th>Entrega</th>
      </tr>
      <?php if($val && is_array($resp->RatedShipment)) foreach($resp->RatedShipment as $obj){ $servicioTipo=$obj->Service->Code;?>
      <tr>
        <td><?php if($val)?><img style="background-color: transparent" src="Img/UPS.png" class="h-25 w-30 img-thumbnail"></td>
        <td><?php if($val) echo servicios($servicioTipo)."\n"; ?> </td>
        <!--<td><?php //if($val) //echo "(Aún se está viendo esto xd)" ?></td>-->
        <td><?php if($val) echo descuento(20,$obj->TotalCharges->MonetaryValue) . " " . $obj->TotalCharges->CurrencyCode . "\n"; ?></td>
        <td><?php if($val) echo "En sucursal" ?></td>
      </tr>
      <?php }else{ if($val) for($i=0;$i<1;$i++){ $servicioTipo=$resp->RatedShipment->Service->Code;?>
        <tr>
        <td><?php if($val)?><img style="background-color: transparent" src="Img/UPS.png" class="h-25 w-30 img-thumbnail"></td>
        <td><?php if($val) echo servicios($servicioTipo)."\n"; ?> </td>
        <!--<td><?php //if($val) //echo "(Aún se está viendo esto xd)" ?></td>-->
        <td><?php if($val) echo descuento(20,$resp->RatedShipment->TotalCharges->MonetaryValue) . " " . $resp->RatedShipment->TotalCharges->CurrencyCode . "\n"; ?></td>
        <td><?php if($val) echo "En sucursal" ?></td>
      </tr>
      <?php } }?>
      </table>
    </div>
  </div>
</p>

</div>

</body>
</html>
