<?php
  session_start();
  if($_POST){
    include 'Tarifas_Logica2.php';
  }
  
?>

<!DOCTYPE html>

<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-grid.css">
<link rel="stylesheet" href="css/bootstrap-grid.min.css">
<link rel="stylesheet" href="css/bootstrap-reboot.css">
<link rel="stylesheet" href="css/bootstrap-reboot.min.css">

<script src="js/bootstrap.bundle.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery-3.4.1.js"></script>

<!--Barra de Navegación-->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">ArtesanoMX</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link" href="Envios_Vista.php">Envíos</a>
      <a class="nav-item nav-link" href="Recoleccion_Vista.php">Recolección</a>
      <a class="nav-item nav-link active" href="Tarifas_Vista2.php">Tarifas<span class="sr-only">(current)</span></a>
    </div>
  </div>
</nav>
  
  <div class="container" >
    <table class="table table-striped">
          <tbody>
             <tr>
                <td colspan="1">
                   <form class="well form-horizontal" method="post" >
                      <fieldset>
                         <div class="form-group" action="Tarifas_Vista2.php" method="post">
                            <label class="col-md-4 control-label">*País a Donde se envía</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group">
                                  <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                  <select class="selectpicker form-control" name="T_shippertoCountry" onChange="mostrar2(this.value);">
                                     <option value="MX">México </option>
                                     <option value="US">Estados Unidos de America</option>
                                  </select>
                               </div>
                            </div>
                         </div>
                         <div class="form-group">
                            <label class="col-md-4 control-label">*Código Postal/ZIP a Donde se envía</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="T_shippertoPc" name="T_shippertoPc" placeholder="Código Postal/ZIP" class="form-control" required="true" value="06470" type="text"></div>
                            </div>
                         </div>
                         <!--
                         <div class="form-group">
                            <label class="col-md-4 control-label">*Estado</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group">
                                    <script type="text/javascript">
                                      function mostrar2(id) {
                                        if (id == "MX") {
                                          $("#MX2").show();
                                          $("#US2").hide();
                                          $("#CND2").hide();
                                        } else if(id == "US"){
                                          $("#MX2").hide();
                                          $("#US2").show();
                                          $("#CND2").hide();
                                        } else if(id == "CA"){
                                          $("#MX2").hide();
                                          $("#US2").hide();
                                          $("#CND2").show();
                                        }
                                      }
                                    </script>
                                    <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                    <div id="MX2">
                                      <select class="selectpicker form-control" name="T_shippertoState">
                                        <option value="">----Sin Seleccionar----</option>
                                       <option value="AG">Aguascalientes</option>
                                       <option value="BJ">Baja California</option>
                                       <option value="BS">Baja California Sur</option>
                                       <option value="CP">Campeche</option>
                                       <option value="CH">Chiapas</option>
                                       <option value="CI">Chihuahua</option>
                                       <option value="CU">Coahuila</option>
                                       <option value="CL">Colima</option>
                                       <option value="DF">CDMX</option>
                                       <option value="DG">Durango</option>
                                       <option value="GJ">Guanajuato</option>
                                       <option value="GR">Guerrero</option>
                                       <option value="HG">Hidalgo</option>
                                       <option value="JA">Jalisco</option>
                                       <option value="EM">México</option>
                                       <option value="MH">Michoacan</option>
                                       <option value="MR">Morelos</option>
                                       <option value="NA">Nayarit</option>
                                       <option value="NL">Nuevo León</option>
                                       <option value="OA">Oaxaca</option>
                                       <option value="PU">Puebla</option>
                                       <option value="QA">Querétaro</option>
                                       <option value="QR">Quintana Roo</option>
                                       <option value="SL">San Luis Potósi</option>
                                       <option value="SI">Sinaloa</option>
                                       <option value="SO">Sonora</option>
                                       <option value="TA">Tabasco</option>
                                       <option value="TM">Tamaulipas</option>
                                       <option value="TL">Tlaxcala</option>
                                       <option value="VL">Veracruz</option>
                                       <option value="YC">Yucatán</option>
                                       <option value="ZT">Zacatecas</option>
                                      </select>
                                    </div>
                                    <div id="US2">
                                      <select class="selectpicker form-control" name="T_shippertoState">
                                        <option value="">----Sin Seleccionar----</option>
                                        <option value="AL">Alabama </option>
                                        <option value="AK">Alaska </option>
                                        <option value="AZ">Arizona </option>
                                        <option value="AR">Arkansas </option>
                                        <option value="CA">California</option>
                                        <option value="NC">Carolina del Norte</option>
                                        <option value="SC">Carolina del Sur</option>
                                        <option value="CO">Colorado </option>
                                        <option value="CT" selected>Conneticut</option>
                                        <option value="ND">Dakota del Norte</option>
                                        <option value="SD">Dakota del Sur</option>
                                        <option value="DC">Distrito de Columbia</option>
                                        <option value="FL">Florida </option>
                                        <option value="GA">Georgia </option>
                                        <option value="HI">Hawaii </option>
                                        <option value="ID">Idaho </option>
                                        <option value="IL">Illinois </option>
                                        <option value="IN">Indiana </option>
                                        <option value="IA">Iowa </option>
                                        <option value="KS">Kansas </option>
                                        <option value="KY">Kentucky </option>
                                        <option value="LA">Lousiana </option>
                                        <option value="ME">Maine </option>
                                        <option value="MD">Maryland </option>
                                        <option value="MA">Massachusetts </option>
                                        <option value="MI">Michigan </option>
                                        <option value="MN">Minnesota </option>
                                        <option value="MS">Mississippo </option>
                                        <option value="MO">Missouri </option>
                                        <option value="MT">Montana </option>
                                        <option value="NE">Nebraska </option>
                                        <option value="NV">Nevada </option>
                                        <option value="NH">New Hampshire</option>
                                        <option value="NJ">Nueva Jersey </option>
                                        <option value="NM">Nuevo México</option>
                                        <option value="NY">Nueva York</option>   
                                        <option value="OH">Ohio </option>
                                        <option value="OK">Oklahoma </option>
                                        <option value="OR">Oregon </option>
                                        <option value="PA">Pennsylvania </option>
                                        <option value="RI">Rhode Island </option>                             
                                        <option value="TN">Tennessee </option>
                                        <option value="TX">Texas </option>
                                        <option value="UT">Utah </option>
                                        <option value="VT">Vermont </option>
                                        <option value="VA">Virginia </option>
                                        <option value="WA">Washington </option>
                                        <option value="WV">Oeste Virginia</option>
                                        <option value="WI">Wisconsin </option>
                                        <option value="WY">Wyoming </option>
                                      </select>
                                    </div>
                                    <div id="CND2">
                                      <select class="selectpicker form-control" name="T_shippertoState">
                                        <option value="">----Sin Seleccionar----</option>
                                        <option value="CND1">Canada</option>
                                      </select>
                                    </div>
                               </div>
                            </div>
                         </div> -->
                         <div class="form-group" action="Tarifas_Vista2.php" method="post">
                            <label class="col-md-4 control-label">*País de Procedencia</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group">
                                  <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                  <select class="selectpicker form-control" name="T_shipperfromCountry" onChange="mostrar3(this.value);">
                                     <option value="MX">México </option>
                                     <option value="US">Estados Unidos de America</option>
                                  </select>
                               </div>
                            </div>
                         </div>
                         <div class="form-group">
                            <label class="col-md-4 control-label">*Código Postal/ZIP de Procedencia</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="T_shipperfromPc" name="T_shipperfromPc" placeholder="Código Postal/ZIP" class="form-control" required="true" value="66215" type="text"></div>
                            </div>
                         </div>
                         <!--
                         <div class="form-group">
                            <label class="col-md-4 control-label">*Estado</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group">
                                    <script type="text/javascript">
                                      function mostrar3(id) {
                                        if (id == "MX") {
                                          $("#MX3").show();
                                          $("#US3").hide();
                                          $("#CND3").hide();
                                        } else if(id == "US"){
                                          $("#MX3").hide();
                                          $("#US3").show();
                                          $("#CND3").hide();
                                        } else if(id == "CA"){
                                          $("#MX3").hide();
                                          $("#US3").hide();
                                          $("#CND3").show();
                                        }
                                      }
                                    </script>
                                    <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                    <div id="MX3">
                                      <select class="selectpicker form-control" name="T_shipperfromState">
                                        <option value="">----Sin Seleccionar----</option>
                                       <option value="AG">Aguascalientes</option>
                                       <option value="BJ">Baja California</option>
                                       <option value="BS">Baja California Sur</option>
                                       <option value="CP">Campeche</option>
                                       <option value="CH">Chiapas</option>
                                       <option value="CI">Chihuahua</option>
                                       <option value="CU">Coahuila</option>
                                       <option value="CL">Colima</option>
                                       <option value="DF">CDMX</option>
                                       <option value="DG">Durango</option>
                                       <option value="GJ">Guanajuato</option>
                                       <option value="GR">Guerrero</option>
                                       <option value="HG">Hidalgo</option>
                                       <option value="JA">Jalisco</option>
                                       <option value="EM">México</option>
                                       <option value="MH">Michoacan</option>
                                       <option value="MR">Morelos</option>
                                       <option value="NA">Nayarit</option>
                                       <option value="NL" selected>Nuevo León</option>
                                       <option value="OA">Oaxaca</option>
                                       <option value="PU">Puebla</option>
                                       <option value="QA">Querétaro</option>
                                       <option value="QR">Quintana Roo</option>
                                       <option value="SL">San Luis Potósi</option>
                                       <option value="SI">Sinaloa</option>
                                       <option value="SO">Sonora</option>
                                       <option value="TA">Tabasco</option>
                                       <option value="TM">Tamaulipas</option>
                                       <option value="TL">Tlaxcala</option>
                                       <option value="VL">Veracruz</option>
                                       <option value="YC">Yucatán</option>
                                       <option value="ZT">Zacatecas</option>
                                      </select>
                                    </div>
                                    <div id="US3">
                                      <select class="selectpicker form-control" name="T_shipperfromState">
                                        <option value="">----Sin Seleccionar----</option>
                                        <option value="AL">Alabama </option>
                                        <option value="AK">Alaska </option>
                                        <option value="AZ">Arizona </option>
                                        <option value="AR">Arkansas </option>
                                        <option value="CA">California</option>
                                        <option value="NC">Carolina del Norte</option>
                                        <option value="SC">Carolina del Sur</option>
                                        <option value="CO">Colorado </option>
                                        <option value="CT">Conneticut</option>
                                        <option value="ND">Dakota del Norte</option>
                                        <option value="SD">Dakota del Sur</option>
                                        <option value="DC">Distrito de Columbia</option>
                                        <option value="FL">Florida </option>
                                        <option value="GA">Georgia </option>
                                        <option value="HI">Hawaii </option>
                                        <option value="ID">Idaho </option>
                                        <option value="IL">Illinois </option>
                                        <option value="IN">Indiana </option>
                                        <option value="IA">Iowa </option>
                                        <option value="KS">Kansas </option>
                                        <option value="KY">Kentucky </option>
                                        <option value="LA">Lousiana </option>
                                        <option value="ME">Maine </option>
                                        <option value="MD">Maryland </option>
                                        <option value="MA">Massachusetts </option>
                                        <option value="MI">Michigan </option>
                                        <option value="MN">Minnesota </option>
                                        <option value="MS">Mississippo </option>
                                        <option value="MO">Missouri </option>
                                        <option value="MT">Montana </option>
                                        <option value="NE">Nebraska </option>
                                        <option value="NV">Nevada </option>
                                        <option value="NH">New Hampshire</option>
                                        <option value="NJ">Nueva Jersey </option>
                                        <option value="NM">Nuevo México</option>
                                        <option value="NY">Nueva York</option>   
                                        <option value="OH">Ohio </option>
                                        <option value="OK">Oklahoma </option>
                                        <option value="OR">Oregon </option>
                                        <option value="PA">Pennsylvania </option>
                                        <option value="RI">Rhode Island </option>                             
                                        <option value="TN">Tennessee </option>
                                        <option value="TX">Texas </option>
                                        <option value="UT">Utah </option>
                                        <option value="VT">Vermont </option>
                                        <option value="VA">Virginia </option>
                                        <option value="WA">Washington </option>
                                        <option value="WV">Oeste Virginia</option>
                                        <option value="WI">Wisconsin </option>
                                        <option value="WY">Wyoming </option>
                                      </select>
                                    </div>
                                    <div id="CND3">
                                      <select class="selectpicker form-control" name="T_shipperfromState">
                                        <option value="">----Sin Seleccionar----</option>
                                        <option value="CND1">Canada</option>
                                      </select>
                                    </div>
                               </div>
                            </div>
                         </div>-->
                         <div class="form-group">
                            <label class="col-md-4 control-label">*Tipo de Paquete</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group">
                                  <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                  <select class="selectpicker form-control" name="T_paquete">
                                     <option value="00">Desconocido </option>
                                     <option value="01">UPS Carta </option>
                                     <option value="02" selected="">Paquete </option>
                                     <option value="03">Tubo </option>
                                     <option value="04">Pak </option>
                                     <option value="21">Caja Exprés </option>
                                     <option value="24">Caja de 25KG </option>
                                     <option value="25">Caja de 10KG </option>
                                     <option value="30">Pallet </option>
                                     <option value="2a">Caja Exprés Chica </option>
                                     <option value="2b">Caja Exprés Mediana </option>
                                     <option value="2c">Caja Exprés Grande </option>
                                  </select>
                               </div>
                            </div>
                         </div>
                       <div class="form-group">
                            <div class="col-md-8 inputGroupContainer">
                            <label class="col-md-4 control-label">*Largo (CM)</label>
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="T_long" name="T_long" placeholder="Longitud" class="form-control" required="true" value="10" type="text">

                            <div class="col-md-8 inputGroupContainer">
                            <label class="col-md-4 control-label">*Ancho (CM)</label>
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="T_anch" name="T_anch" placeholder="Ancho" class="form-control" required="true" value="10" type="text">
                            
                            <div class="col-md-8 inputGroupContainer">
                            <label class="col-md-4 control-label">*Alto (CM)</label>
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="T_alto" name="T_alto" placeholder="Alto" class="form-control" required="true" value="10" type="text">
                                  </div>
                                  </div>
                                  </div>
                            </div> 
                            </div> 
                            </div>   
                         </div>
                         <div class="form-group">
                            <label class="col-md-4 control-label">*Peso (KG)</label>
                            <div class="col-md-8 inputGroupContainer">
                               <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="T_peso" name="T_peso" placeholder="Peso" class="form-control" required="true" value="10" type="text">
                                </div>
                            </div>
                         </div>

                      </fieldset>
                      <button type="submit" value="Entrar" id="enviar" class="btn btn-success">Enviar</button>
                   </form>
                </td>
                </tbody>
                </table>
  </div>



</html>