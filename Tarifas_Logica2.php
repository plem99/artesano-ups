<?php
  //include 'Tarifas_Datos.php';
  //Configuración
  $access = "ED715201D9516E11";
  $userid = "artesanomx";
  $passwd = "Mexico-05";
  $wsdl = "Archivos_UPS\RatingPACKAGE\PACKAGEWebServices\SCHEMA-WSDLs\RateWS.wsdl";
  $operation = "ProcessRate";
  $endpointurl = 'https://wwwcie.ups.com/webservices/Rate';
  $outputFileName = "XOLTResult.xml";

  function estados($pais, $codigo){
    $estado;
    switch ($pais){
      case 'MX':
        if($codigo>='00001' && $codigo<='16999'){
          $estado = 'DF';
        }elseif($codigo>='20000' && $codigo<='20999'){
          $estado = 'AG';
        }elseif($codigo>='21000' && $codigo<='22999'){
          $estado = 'BJ';
        }elseif($codigo>='23000' && $codigo<='23999'){
          $estado = 'BS';
        }elseif($codigo>='24000' && $codigo<='24999'){
          $estado = 'CP';
        }elseif($codigo>='25000' && $codigo<='27999'){
          $estado = 'CU';
        }elseif($codigo>='28000' && $codigo<='28999'){
          $estado = 'CL';
        }elseif($codigo>='29000' && $codigo<='30999'){
          $estado = 'CH';
        }elseif($codigo>='31000' && $codigo<='16999'){
          $estado = 'CI';
        }elseif($codigo>='34000' && $codigo<='35999'){
          $estado = 'DG';
        }elseif($codigo>='36000' && $codigo<='38999'){
          $estado = 'GJ';
        }elseif($codigo>='39000' && $codigo<='41999'){
          $estado = 'GR';
        }elseif($codigo>='42000' && $codigo<='43999'){
          $estado = 'HG';
        }elseif($codigo>='44000' && $codigo<='49999'){
          $estado = 'JA';
        }elseif($codigo>='50000' && $codigo<='57999'){
          $estado = 'EM';
        }elseif($codigo>='58000' && $codigo<='61999'){
          $estado = 'MH';
        }elseif($codigo>='62000' && $codigo<='62999'){
          $estado = 'MR';
        }elseif($codigo>='63000' && $codigo<='63999'){
          $estado = 'NA';
        }elseif($codigo>='64000' && $codigo<='67999'){
          $estado = 'NL';
        }elseif($codigo>='68000' && $codigo<='71999'){
          $estado = 'OA';
        }elseif($codigo>='72000' && $codigo<='75999'){
          $estado = 'PU';
        }elseif($codigo>='76000' && $codigo<='76999'){
          $estado = 'QA';
        }elseif($codigo>='77000' && $codigo<='77999'){
          $estado = 'QR';
        }elseif($codigo>='78000' && $codigo<='79999'){
          $estado = 'SL';
        }elseif($codigo>='80000' && $codigo<='82999'){
          $estado = 'SI';
        }elseif($codigo>='83000' && $codigo<='83399'){
          $estado = 'SO';
        }elseif($codigo>='83500' && $codigo<='85999'){
          $estado = 'SO';
        }elseif($codigo>='86000' && $codigo<='86999'){
          $estado = 'TA';
        }elseif($codigo>='87000' && $codigo<='89999'){
          $estado = 'TM';
        }elseif($codigo>='90000' && $codigo<='90999'){
          $estado = 'TL';
        }elseif($codigo>='91000' && $codigo<='96999'){
          $estado = 'VL';
        }elseif($codigo>='97000' && $codigo<='97999'){
          $estado = 'YC';
        }elseif($codigo>='98000' && $codigo<='99999'){
          $estado = 'ZT';
        }
        break;
      case 'US':
        if($codigo>='35004' && $codigo<='36925'){
          $estado = 'AL';
        }elseif($codigo>='99501' && $codigo<='99950'){
          $estado = 'AK';
        }elseif($codigo>='85001' && $codigo<='86556'){
          $estado = 'AZ';
        }elseif($codigo>='71601' && $codigo<='72959'){
          $estado = 'AR';
        }elseif($codigo>='90001' && $codigo<='96162'){
          $estado = 'CA';
        }elseif($codigo>='80001' && $codigo<='81658'){
          $estado = 'CO';
        }elseif($codigo>='06001' && $codigo<='06928'){
          $estado = 'CT';
        }elseif($codigo>='19901' && $codigo<='19905'){
          $estado = 'DE';
        }elseif($codigo>='20001' && $codigo<='20020'){
          $estado = 'DC';
        }elseif($codigo>='32501' && $codigo<='33190'){
          $estado = 'FL';
        }elseif($codigo>='30301' && $codigo<='30381'){
          $estado = 'GA';
        }elseif($codigo>='96801' && $codigo<='96830'){
          $estado = 'HI';
        }elseif($codigo>='83254' && $codigo<='83254'){
          $estado = 'ID';
        }elseif($codigo>='60601' && $codigo<='62709'){
          $estado = 'IL';
        }elseif($codigo>='46201' && $codigo<='46209'){
          $estado = 'IN';
        }elseif($codigo>='50301' && $codigo<='52809'){
          $estado = 'IA';
        }elseif($codigo>='67201' && $codigo<='67221'){
          $estado = 'KS';
        }elseif($codigo>='41701' && $codigo<='41702'){
          $estado = 'KY';
        }elseif($codigo>='70112' && $codigo<='70119'){
          $estado = 'LA';
        }elseif($codigo>='04032' && $codigo<='04034'){
          $estado = 'ME';
        }elseif($codigo>='21201' && $codigo<='21237'){
          $estado = 'MD';
        }elseif($codigo>='02101' && $codigo<='02137'){
          $estado = 'MA';
        }elseif($codigo>='49036' && $codigo<='49735'){
          $estado = 'MI';
        }elseif($codigo>='55801' && $codigo<='55808'){
          $estado = 'MN';
        }elseif($codigo>='39530' && $codigo<='39535'){
          $estado = 'MS';
        }elseif($codigo>='63101' && $codigo<='63141'){
          $estado = 'MO';
        }elseif($codigo>='59044' && $codigo<='59044'){
          $estado = 'MT';
        }elseif($codigo>='68901' && $codigo<='68902'){
          $estado = 'NE';
        }elseif($codigo>='89501' && $codigo<='89513'){
          $estado = 'NV';
        }elseif($codigo>='03217' && $codigo<='03217'){
          $estado = 'NH';
        }elseif($codigo>='07039' && $codigo<='07039'){
          $estado = 'NJ';
        }elseif($codigo>='87500' && $codigo<='87506'){
          $estado = 'NM';
        }elseif($codigo>='10001' && $codigo<='10048'){
          $estado = 'NY';
        }elseif($codigo>='27565' && $codigo<='27565'){
          $estado = 'NC';
        }elseif($codigo>='58282' && $codigo<='58282'){
          $estado = 'ND';
        }elseif($codigo>='44101' && $codigo<='44179'){
          $estado = 'OH';
        }elseif($codigo>='74101' && $codigo<='74110'){
          $estado = 'OK';
        }elseif($codigo>='97201' && $codigo<='97225'){
          $estado = 'OR';
        }elseif($codigo>='15201' && $codigo<='15244'){
          $estado = 'PA';
        }elseif($codigo>='02840' && $codigo<='02841'){
          $estado = 'RI';
        }elseif($codigo>='29020' && $codigo<='29020'){
          $estado = 'SC';
        }elseif($codigo>='57401' && $codigo<='57402'){
          $estado = 'SD';
        }elseif($codigo>='37201' && $codigo<='37222'){
          $estado = 'TN';
        }elseif($codigo>='78701' && $codigo<='78705'){
          $estado = 'TX';
        }elseif($codigo>='84321' && $codigo<='84323'){
          $estado = 'UT';
        }elseif($codigo>='05751' && $codigo<='05751'){
          $estado = 'VT';
        }elseif($codigo>='24517' && $codigo<='24517'){
          $estado = 'VA';
        }elseif($codigo>='98004' && $codigo<='98009'){
          $estado = 'WA';
        }elseif($codigo>='25813' && $codigo<='25813'){
          $estado = 'WV';
        }elseif($codigo>='53201' && $codigo<='53228'){
          $estado = 'WI';
        }elseif($codigo>='82941' && $codigo<='82941'){
          $estado = 'WY';
        }
        break;
    }
    return $estado;
  }

  function servicios($servicioTipo){
    switch ($servicioTipo) {
      case '70':
        $valor='UPS Access Point Economy';
      break;
      case '08':
        $valor='UPS Expedited';
      break;
      case '07':
        $valor='UPS Exprés';
      break;
      case '11':
        $valor='UPS Estándar';
      break;
      case '54':
        $valor='UPS Worldwide Express Plus';
      break;
      case '65':
        $valor='UPS Worldwide Saver';
      break;
    }
    return $valor;
  } 

  function descuento($desc, $valor){
    $valFinal=($desc*$valor)/100;
    $valFinal=$valor-$valFinal;
    return $valFinal;
  }

  function pesoVol($largo, $alto, $ancho){
    $pesoVol=($largo * $alto * $ancho)/5000;
    return $pesoVol;
  }  

   function processRate($val){
      //Saber si es internacional o nacional
      $nac=$val;
      //Opciones
      $opcion='Shop'; 
      //TipoRecolección
      $recoleccion='01'; 
      //Transportista
      $transportistaNomb = array(' ', ' ', ' ');
      $transportistaNum= ' ';
      $transportistaDirecc = array(' ', ' ', ' ');
      $transportistaCiudad = array(' ', ' ', ' ');
      if($nac){
        $transportistaPais = array('MX', 'MX', 'MX');
      }else{
        $transportistaPais = array($_POST["T_shipperfromCountry"], $_POST["T_shippertoCountry"], $_POST["T_shipperfromCountry"]);
      }
      $transportistaCodPost = array($_POST["T_shipperfromPc"], $_POST["T_shippertoPc"], $_POST["T_shipperfromPc"]);
      $transportistaEstado = array(' ', ' ', ' ');
      $transportistaRef= ' ';
      //Servicios
      $servicios= '11';

      //Tipo de paquete
      $paquete='02';
      
      $longitud=$_POST["T_long"];
      $ancho=$_POST["T_anch"];
      $altura=$_POST["T_alto"];
      $medida= 'CM';
      //Peso
      $peso=$_POST["T_peso"];
      $tipoPeso= 'KGS';

      //Saber a que estado se refiere mediante el código postal, tomando en cuenta el pais
      $transportistaEstado[0] = estados($transportistaPais[0], $transportistaCodPost[0]);
      $transportistaEstado[1] = estados($transportistaPais[1], $transportistaCodPost[1]);
      $transportistaEstado[2] = estados($transportistaPais[2], $transportistaCodPost[2]);
      //var_dump($transportistaEstado[0]);
          //var_dump($transportistaEstado[1]);
      //var_dump($transportistaEstado[2]);
      //Imprimir valores obtenidos
      /*echo "Opcion: " . var_dump($opcion) . "\n";
      echo "Tipo de Recolección: " . var_dump($recoleccion) . "\n";
      echo "Nombre del Transportista: " . var_dump($transportistaNomb) . "\n";
      echo "Número del Transportista: " . var_dump($transportistaNum) . "\n";
      echo "Dirección del Transportista: " . var_dump($transportistaDirecc) . "\n";
      echo "Ciudad del Transportista: " . var_dump($transportistaCiudad) . "\n";
      echo "Pais del Transportista: " . var_dump($transportistaPais) . "\n";
      echo "Código Postal del Transportista: " . var_dump($transportistaCodPost) . "\n";
      echo "Estado del Transportista: " . var_dump($transportistaEstado) . "\n";
      echo "Referencias del Envío: " . var_dump($transportistaRef) . "\n";
      echo "Tipo de Servicio: " . var_dump($servicios) . "\n";
      echo "Tipo de Paquete: " . var_dump($paquete) . "\n";
      echo "Longitud: " . var_dump($longitud) . "\n";
      echo "Ancho: " . var_dump($ancho) . "\n";
      echo "Altura: " . var_dump($altura) . "\n";
      echo "Tipo de Medida: " . var_dump($medida) . "\n";
      echo "Peso: " . var_dump($peso) . "\n";
      echo "Tipo de Peso: " . var_dump($tipoPeso) . "\n";*/
      
      //create soap request
      //Opción
      $option['RequestOption'] = $opcion;
      $request['Request'] = $option;

      //Tipo de recolección
      $pickuptype['Code'] = $recoleccion;
      $pickuptype['Description'] = 'Opcion de Recolección';
      $request['PickupType'] = $pickuptype;

      //Opcional
      $customerclassification['Code'] = '01';
      $customerclassification['Description'] = 'Classfication';
      $request['CustomerClassification'] = $customerclassification;

      //Transportista
      $shipper['Name'] = $transportistaNomb[0];
      $shipper['ShipperNumber'] = $transportistaNum;
      $address['AddressLine'] = $transportistaDirecc[0];
      $address['City'] = $transportistaCiudad[0];
      $address['StateProvinceCode'] = $transportistaEstado[0];
      $address['PostalCode'] = $transportistaCodPost[0];
      $address['CountryCode'] = $transportistaPais[0];
      $shipper['Address'] = $address;
      $shipment['Shipper'] = $shipper;

      //A Donde se Enviara
      $shipto['Name'] = $transportistaNomb[1];
      $addressTo['AddressLine'] = $transportistaDirecc[1];
      $addressTo['City'] = $transportistaCiudad[1];
      $addressTo['StateProvinceCode'] = $transportistaEstado[1];
      $addressTo['PostalCode'] = $transportistaCodPost[1];
      $addressTo['CountryCode'] = $transportistaPais[1];
      $addressTo['ResidentialAddressIndicator'] = $transportistaRef;
      $shipto['Address'] = $addressTo;
      $shipment['ShipTo'] = $shipto;

      //De donde Vino
      $shipfrom['Name'] = $transportistaNomb[2];
      $addressFrom['AddressLine'] = $transportistaDirecc[2];
      $addressFrom['City'] = $transportistaCiudad[2];
      $addressFrom['StateProvinceCode'] = $transportistaEstado[2];
      $addressFrom['PostalCode'] = $transportistaCodPost[2];
      $addressFrom['CountryCode'] = $transportistaPais[2];
      $shipfrom['Address'] = $addressFrom;
      $shipment['ShipFrom'] = $shipfrom;

      //Servicio (Plan de Envío)
      $service['Code'] = $servicios;
      $service['Description'] = 'Código del Servicio';
      $shipment['Service'] = $service;

      //Tipo de Paquete a enviar
      $packaging1['Code'] = $paquete;
      $packaging1['Description'] = 'Rate';
      $package1['PackagingType'] = $packaging1;
      $dunit1['Code'] = $medida;
      $dunit1['Description'] = 'Tipo de Medición';
      $dimensions1['Length'] = $longitud;
      $dimensions1['Width'] = $ancho;
      $dimensions1['Height'] = $altura;
      $dimensions1['UnitOfMeasurement'] = $dunit1;
      $package1['Dimensions'] = $dimensions1;
      $punit1['Code'] = $tipoPeso;
      $punit1['Description'] = 'Tipo de Peso';
      $packageweight1['Weight'] = $peso;
      $packageweight1['UnitOfMeasurement'] = $punit1;
      $package1['PackageWeight'] = $packageweight1;

      $shipment['Package'] = array($package1);
      $shipment['ShipmentServiceOptions'] = '';
      $shipment['LargePackageIndicator'] = '';
      $request['Shipment'] = $shipment;
      //echo "Request.......\n";
      //print_r($request);
      //echo "\n\n";
      return $request;
  }

  try
  {

    $mode = array
    (
         'soap_version' => 'SOAP_1_1',  // use soap 1.1 client
         'trace' => 1
    );

    // initialize soap client
    $client = new SoapClient($wsdl , $mode);

    //set endpoint url
    $client->__setLocation($endpointurl);


    //create soap header
    $usernameToken['Username'] = $userid;
    $usernameToken['Password'] = $passwd;
    $serviceAccessLicense['AccessLicenseNumber'] = $access;
    $upss['UsernameToken'] = $usernameToken;
    $upss['ServiceAccessToken'] = $serviceAccessLicense;

    $header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0','UPSSecurity',$upss);
    $client->__setSoapHeaders($header);


    //get response
    $resp = $client->__soapCall($operation ,array(processRate($nac)));

    //get status
    if ($resp->Response->ResponseStatus->Description) {
      //$valor;
      //Para saber si hay mas de una opcion
      /*if($resp->RatedShipment[0]->Service->Code){
        foreach($resp->RatedShipment as $obj){
          $servicioTipo=$obj->Service->Code;
          servicios($servicioTipo);
        }
      }elseif($resp->RatedShipment->Service->Code){//Para saber si hay solo una opcion
        $servicioTipo=$resp->RatedShipment->Service->Code;
        servicios($servicioTipo);
      }*/
      //echo "\n\n";
      //echo "Prueba Satisfactoria"."\n";
      
      
    }else{
      echo "Prueba Fallida\n";
    }
    //echo "Response Status: " . $resp->Response->ResponseStatus->Description ."\n";

    //save soap request and response to file
    $fw = fopen($outputFileName , 'w');
    fwrite($fw , "Request: \n" . $client->__getLastRequest() . "\n");
    fwrite($fw , "Response: \n" . $client->__getLastResponse() . "\n");
    fclose($fw);

  }
  catch(Exception $ex)
  {
    print_r ($ex);
  }
?>